// ==UserScript==
// @name        Collect Fuel Shortcut
// @namespace   http://userscripts.xcom-alliance.info/
// @description Allows a keypress to collect fuel
// @author      Miche (Orion) / Sparkle (Artemis)
// @version     1.0
// @include     http*://*.pardus.at/main.php*
// @exclude     http*://chat.pardus.at*
// @exclude     http*://forum.pardus.at*
// @updateURL 	http://userscripts.xcom-alliance.info/fuel_collector_shortcut/pardus_fuel_collector_shortcut.meta.js
// @downloadURL http://userscripts.xcom-alliance.info/fuel_collector_shortcut/pardus_fuel_collector_shortcut.user.js
// @icon 		http://userscripts.xcom-alliance.info/fuel_collector_shortcut/icon.png
// ==/UserScript==

/*****************************************************************************************
	Version Information

	07/98/2012 (Version 1.0)
	  - Released this simple keyboard shortcut

	When on a fuel tile, press "h" once to collect fuel. You can change this key
	choice by my updating the KEY_PRESS_FOR_FUEL setting directly below.

*****************************************************************************************/

var KEY_PRESS_FOR_FUEL = 'h';

/*****************************************************************************************
	There is no need to modify anything further in this file
*****************************************************************************************/
function handleKeyPressForFuel(e) {
	if (e.ctrlKey || e.target.nodeName == 'INPUT' || e.target.nodeName == 'TEXTAREA') return;
	if (String.fromCharCode(e.which).toLowerCase() == KEY_PRESS_FOR_FUEL.toLowerCase() && document.getElementById('aCmdCollect') && document.getElementById('aCmdCollect').textContent.indexOf('fuel')>-1) document.getElementById('aCmdCollect').click();
};

window.addEventListener("keypress", handleKeyPressForFuel);